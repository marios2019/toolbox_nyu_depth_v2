import subprocess

frame_list_file = "./frame_list.txt"

matlab_script = 'save_synched_projected_frames_parallel';

launch_generate_frame_list = "matlab -nodesktop -nosplash -r 'generateFrameList=1;%s;exit;'" % (matlab_script)

launch_save_frame_batch = "matlab -nodesktop -nosplash -r 'batchId=%d;batchSize=%d;%s;exit;' &"

testing = False; # If true just messages will be displayed

#print(launch_generate_frame_list)
#print(launch_save_frame_batch)

subprocess.call(launch_generate_frame_list, shell=True)

batchSize = 32;
# Read the frame list to determine how many workers needed
with open(frame_list_file) as frames:
   line = frames.readline()
   cnt = 1
   # Loop through target frames
   while line:
       #print("Line {}: {}".format(cnt, line.strip()))
       frame = line.strip()

       line = frames.readline()
       cnt += 1

workers = cnt / batchSize;
if (cnt % batchSize != 0):
  workers += 1

# Spawn the workers
for x in range(0, workers):
  spawn_command = launch_save_frame_batch % (x+1, batchSize, matlab_script)
  #print(spawn_command)
  subprocess.call(spawn_command, shell=True)

